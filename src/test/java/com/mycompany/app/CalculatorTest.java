package com.mycompany.app;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.beans.Transient;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class CalculatorTest {

	private static App app;
	@BeforeClass
	public static void initApp() {
		app = new App();
	}

	@Before
	public void beforeEachTest() {
		System.out.println("This is executed before each Test");
	}

	@After
	public void afterEachTest() {
		System.out.println("This is exceuted after each Test");
	}

	@Test
	public void testApp(){
		int testNum = 4;
		app.setIt(testNum);
		assertEquals(app.testVal, testNum);
	}

	@Test
	public void testSum() {
		int result = 3+4;

		assertEquals(7, result);
	}

	@Test
	public void testDivison() {
		try {
			int result = 10/2;

			assertEquals(5, result);
		} catch (Exception e) {
			e.printStackTrace(System.err);
		}
	}

	@Test(expected = Exception.class)
	public void testDivisionException() throws Exception {
		double test = 10/0;
	}

	@Ignore
	@Test
	public void testEqual() {
		boolean result = false;

		assertFalse(result);
	}

	@Ignore
	@Test
	public void testSubstraction() {
		int result = 10 - 1;

		assertTrue(result == 9);
	}
}
